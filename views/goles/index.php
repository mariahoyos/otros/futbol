<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Goles';
$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->user->isGuest){ ?>
<div class="goles-index">

    <h1><?= Html::encode($this->title) ?></h1>

<!--    <p>
        <?= Html::a('Create Goles', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_gol',
            'codigo_jugador_gol',
            'codigo_jugador_asistencia',
            'codigo_partido',
            'minuto',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php } ?>

<?php if (!Yii::$app->user->isGuest){ ?>

<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">

<div class="goles-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Gol', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_gol',
                       [
                'attribute'=>'goleador',
                'value'=>function($model) { if ($model->codigoJugadorGol['nombre'] != null)  {
                return $model->codigoJugadorGol['nombre'].' '.$model->codigoJugadorGol['apellidos'];} else{$model->codigoJugadorGol[''];}}
            ],
                       [
                'attribute'=>'asistente',
                'value'=>function($model) { if ($model->codigoJugadorAsistencia['nombre'] != null)  {
                return $model->codigoJugadorAsistencia['nombre'].' '.$model->codigoJugadorAsistencia['apellidos'];} else{$model->codigoJugadorAsistencia['null'];}}
            ],
                       [
                'attribute'=>'partido',
                'value'=>function($model) {
                return $model->codigoPartido->codigoEquipoCasa->nombre_equipo.' - '.$model->codigoPartido->codigoEquipoFuera->nombre_equipo;}
            ],
            'minuto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
</div>
<?php } ?>