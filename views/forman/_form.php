<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Forman */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="forman-form">
    
    <p class="text-right">*Campos obligatorios</p>
    
    <?php $form = ActiveForm::begin(); ?>
    
    <?php
    
    $listadeequipos= ArrayHelper::map($model_equipos, 'codigo_equipo', 'nombre_equipo');?>
    
    
    <?= $form->field($model, 'codigo_equipo')->dropDownList($listadeequipos)->label('Equipo')?>

    
    <?php
    
    $listadejugadores= ArrayHelper::map($model_jugadores, 'codigo_jugador',
            function($model) {
                return $model['nombre'].' '.$model['apellidos'];
            })
    ?>
    
    <?= $form->field($model, 'codigo_jugador')->dropDownList ($listadejugadores)->label('Jugador')?>
    
    <?php

//Calendario
echo $form->field($model, 'fecha_alta')->label('Fecha de alta*')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Enter birth date ...'],
    'pluginOptions' => [
        'autoclose'=>true
    ]
]);
?>

<?php

//Calendario
echo $form->field($model, 'fecha_baja')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Enter birth date ...'],
    'pluginOptions' => [
        'autoclose'=>true
    ]
]);
?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
