<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Forman';
$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->user->isGuest){ ?>
<div class="forman-index">

    <h1><?= Html::encode($this->title) ?></h1>

<!--    <p>
        <?= Html::a('Create Forman', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_forman',
            'codigo_equipo',
            'codigo_jugador',
            'fecha_alta',
            'fecha_baja',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php } ?>

<?php if (!Yii::$app->user->isGuest){ ?>

<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">

<div class="forman-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Forman', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_forman',
                                    [
                'attribute'=>'equipo',
                'value'=>function($model) {
                return $model->codigoEquipo->nombre_equipo;}
            ],
                        [
                'attribute'=>'jugador',
                'value'=>function($model) {
                return $model->codigoJugador->nombre.' '.$model->codigoJugador->apellidos;}
            ],
            'fecha_alta',
            'fecha_baja',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
</div>
<?php } ?>