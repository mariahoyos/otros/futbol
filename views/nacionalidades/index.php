<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nacionalidades';
$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->user->isGuest){ ?>
<div class="nacionalidades-index">

    <h1><?= Html::encode($this->title) ?></h1>

<!--    <p>
        <?= Html::a('Create Nacionalidades', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_nacionalidad',
            'codigo_jugador',
            'nombre_nacionalidad',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php } ?>

<?php if (!Yii::$app->user->isGuest){ ?>

<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">

<div class="nacionalidades-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Nacionalidad', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_nacionalidad',
                                    [
                'attribute'=>'jugador',
                'value'=>function($model) {
                return $model->codigoJugador->nombre.' '.$model->codigoJugador->apellidos;}
            ],
            'nombre_nacionalidad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
</div>
<?php } ?>