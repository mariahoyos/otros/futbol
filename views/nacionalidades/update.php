<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Nacionalidades */

$this->title = 'Actualizar Nacionalidad: ' . $model->codigo_nacionalidad;
$this->params['breadcrumbs'][] = ['label' => 'Nacionalidades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_nacionalidad, 'url' => ['view', 'id' => $model->codigo_nacionalidad]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">
<div class="nacionalidades-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_jugadores' => $model_jugadores,
    ]) ?>

</div>
</div>
