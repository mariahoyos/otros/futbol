<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Competiciones';
$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->user->isGuest){ ?>

<div class="competiciones-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
<!--    <p>
        <?= Html::a('Create Competiciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' =>  [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_competicion',
            'nombre',
            'ganador',
            'tipo',
            'num_equipos',
            'anio_inicio',
            'anio_fin',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
</div>

<?php } ?>

<?php if (!Yii::$app->user->isGuest){ ?>

<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">

<div class="competiciones-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <p>
        <?= Html::a('Crear Competicion', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_competicion',
            'nombre',
            'ganador',
            'tipo',
            'num_equipos',
            'anio_inicio',
            'anio_fin',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
</div>
</div>
<?php } ?>