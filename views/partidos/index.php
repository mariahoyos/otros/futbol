<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Partidos';
$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->user->isGuest){ ?>
<div class="partidos-index">

    <h1><?= Html::encode($this->title) ?></h1>

<!--    <p>
        <?= Html::a('Create Partidos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_partido',
            'codigo_competicion',
            'jornada',
            'estadio',
            'fecha',
            'hora',
            //'codigo_equipo_casa',
            //'codigo_equipo_fuera',
            //'resultado_equipo_casa',
            //'resultado_equipo_fuera',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php } ?>

<?php if (!Yii::$app->user->isGuest){ ?>

<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">

<div class="partidos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Partido', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'codigo_partido',

            [
                'attribute'=>'competicion',
                'value'=>function($model) {
                return $model->codigoCompeticion->nombre;}
            ],
            
            'jornada',
            'estadio',
            'fecha',
            'hora',
            
            [
                'attribute'=>'equipo casa',
                'value'=>function($model) {
                return $model->codigoEquipoCasa->nombre_equipo;}
            ],
            
            [
                'attribute'=>'equipo fuera',
                'value'=>function($model) {
                return $model->codigoEquipoFuera->nombre_equipo;}
            ],
            
            'resultado_equipo_casa',
            'resultado_equipo_fuera',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
</div>
<?php } ?>