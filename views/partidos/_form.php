<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\time\TimePicker; 
use kartik\datecontrol\DateControl;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Partidos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="partidos-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php

    $listadecompeticiones= ArrayHelper::map($model_competiciones, 'codigo_competicion', 'nombre');?>   
    
    <?= $form->field($model, 'codigo_competicion')->dropDownList($listadecompeticiones)->label('Competicion')?>
    
    <?php $jornada = [
        '1' => '1', 
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10',
        '11' => '11',
        '12' => '12',
        '13' => '13',
        '14' => '14',
        '15' => '15',
        '16' => '16',
        '17' => '17',
        '18' => '18',
        '19' => '19',
        '20' => '20',
        '21' => '21',
        '22' => '22',
        '23' => '23',
        '24' => '24',
        '25' => '25',
        '26' => '26',
        '27' => '27',
        '28' => '28',
        '29' => '29',
        '30' => '30',
        '31' => '31',
        '32' => '32',
        '33' => '33',
        '34' => '34',
        '35' => '35',
        '36' => '36',
        '37' => '37',
        '38' => '38',
        'Final' => 'Final',
        'Semifinal' => 'Semifinal',
        'Cuartos de final' => 'Cuartos de final',
        'Octavos de final' => 'Octavos de final',
        'Dieciseisavos de final' => 'Dieciseisavos de final',
        'null' => 'null'
        
    ];
?>
    <?= $form->field($model, 'jornada')->dropdownList($jornada)->label('Jornada') ?>

    <?php

    $listadeestadios= ArrayHelper::map($model_equipos, 'estadio', 'estadio');?>   
    
    <?= $form->field($model, 'estadio')->dropDownList($listadeestadios)->label('Estadios')?>

        



<?php
//Calendario
echo $form->field($model, 'fecha')->widget(DateControl::classname(), [
    'type' => 'date',
    'ajaxConversion' => true,
    'autoWidget' => true,
    'widgetClass' => '',
    'displayFormat' => 'php:d-F-Y',
    'saveFormat' => 'php:Y-m-d',
    'saveTimezone' => 'Europe/Brussels',
    'displayTimezone' => 'Europe/Brussels',
//    'saveOptions' => [
//        'label' => 'Input saved as: ',
//        'type' => 'text',
//        'readonly' => true,
//        'class' => 'hint-input text-muted'
//    ],
    'widgetOptions' => [
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'php:d-F-Y'
        ]
    ],
    'language' => 'es'
]);
?>
    

<?php 
//Hora
 echo $form->field($model, 'hora')->widget(DateControl::classname(), [
    'type' => 'time',
    'ajaxConversion' => true,
    'autoWidget' => true,
    'widgetClass' => '',
    'displayFormat' => 'php:h:i:s A',
    'saveFormat' => 'php:H:i:s',
    'saveTimezone' => 'Europe/Brussels',
    'displayTimezone' => 'Europe/Brussels',
//    'saveOptions' => [
//        'label' => 'Input saved as: ',
//        'type' => 'text',
//        'readonly' => true,
//        'class' => 'hint-input text-muted'
//    ],
    'widgetOptions' => [
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'php:h:i:s A'
        ]
    ],
    'language' => 'es'
]);
 
 ?>
    <?php

    $listadeequipos= ArrayHelper::map($model_equipos, 'codigo_equipo', 'nombre_equipo');?>  

    <?= $form->field($model, 'codigo_equipo_casa')->dropdownList($listadeequipos)->label('Equipo de casa') ?>

    <?php

    $listadeequipos= ArrayHelper::map($model_equipos, 'codigo_equipo', 'nombre_equipo');?>
    
    <?= $form->field($model, 'codigo_equipo_fuera')->dropdownList($listadeequipos)->label('Equipo de fuera') ?>

    <?= $form->field($model, 'resultado_equipo_casa')->textInput() ?>

    <?= $form->field($model, 'resultado_equipo_fuera')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
