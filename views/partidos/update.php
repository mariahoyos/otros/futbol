<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Partidos */

$this->title = 'Actualizar Partido: ' . $model->codigo_partido;
$this->params['breadcrumbs'][] = ['label' => 'Partidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_partido, 'url' => ['view', 'id' => $model->codigo_partido]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">
<div class="partidos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_competiciones' => $model_competiciones,
        'model_equipos' => $model_equipos,
    ]) ?>

</div>
</div>