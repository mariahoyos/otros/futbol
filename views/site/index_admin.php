<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->params['fluid'] = true;
$this->title = 'Administrador';
?>
<!--inicio del header-->
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class= "jumbotron text-center">
    <h1><b>BIENVENIDO SEÑOR ADMINISTRADOR<b/></h1>
</div>

<div class="container<?= $this->params['fluid'] ? '-fluid' : '' ?> "> 
    <div class="container">
        <div class="text-center">
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-center">
                        <div class="thumbnail">
                            <div class="text-right alineacion"><h5>*Se necesita completar los 3 apartados</h5></div>
                                <div class="caption">
                                    <p><h2 class='h2header'>PARTIDOS</h2></p>                           
                                    <div class="container-fluid">                                   
                                        <div class="row">
                                        
                                        <div class="col-md-4">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h4>Nuevo partido</h4>
                                                    <?= Html::a('Añadir', ['partidos/create'], ['class' => 'btn btn-success']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h4>Participacion de los jugadores en el partido</h4>
                                                    <?= Html::a('Añadir', ['juegan/create'], ['class' => 'btn btn-success']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h4>Goles del partido</h4>
                                                    <?= Html::a('Añadir', ['goles/create'], ['class' => 'btn btn-success']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-center">
                        <div class="thumbnail">
                            <div class="text-right alineacion"><h5>*Se necesita completar los 3 apartados</h5></div>
                            <div class="caption">
                                <p><h2 class='h2header'>JUGADORES</h2></p>                           
                                <div class="container-fluid">                                   
                                    <div class="row"> 
                                        
                                        <div class="col-md-4">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h4>Nuevo jugador</h4>
                                                    <?= Html::a('Añadir', ['jugadores/create'], ['class' => 'btn btn-success']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h4>Jugador en un equipo</h4>
                                                    <?= Html::a('Añadir', ['forman/create'], ['class' => 'btn btn-success']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h4>Nacionalidad del jugador</h4>
                                                    <?= Html::a('Añadir', ['nacionalidades/create'], ['class' => 'btn btn-success']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-center">
                        <div class="thumbnail">
                            <div class="text-right alineacion"><h5></h5></div>
                                <div class="caption">
                                    <h2 class='h2header'>COMPETICIONES</h2>                           
                                    <div class="container-fluid">                                   
                                        <div class="row">
                                        
                                        <div class="col-md-2 col-md-offset-5">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h4 class="text-center">Nueva competicion</h4>
                                                    <?= Html::a('Añadir', ['competiciones/create'], ['class' => 'btn btn-success']) ?>
                                                </div>
                                            </div>
                                        </div>
 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-center">
                        <div class="thumbnail">
                            <div class="text-right alineacion"><h5></h5></div>
                                <div class="caption">
                                    <h2 class='h2header'>EQUIPOS</h2>                           
                                    <div class="container-fluid">                                   
                                        <div class="row">
                                        
                                        <div class="col-md-2 col-md-offset-5">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h4>Nuevo equipo</h4>
                                                    <?= Html::a('Añadir', ['equipos/create'], ['class' => 'btn btn-success']) ?>
                                                </div>
                                            </div>
                                        </div>
 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div> 
    </div>
</div>  


