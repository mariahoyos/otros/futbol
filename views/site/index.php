<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
$this->params['fluid'] = true;
$this->title = 'Estadisticas de futbol';

?>
<!--inicio del header-->
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

    <div class= "jumbotron filas" >
        <h2><b><?=Html::img('@web/images/barcelona.png',["alt=barcelona",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/alaves.png',["alt=alaves",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/bilbao.png',["alt=bilbao",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/atletico.png',["alt=atletico",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/eibar.png',["alt=eibar",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/getafe.png',["alt=getafe",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/leganes.png',["alt=leganes",'class'=>'escudo']);?><b/></h2> 
        <h2><b><?=Html::img('@web/images/español.png',["alt=español",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/levante.png',["alt=levante",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/celta.png',["alt=celta",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/betis.png',["alt=betis",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/osasuna.png',["alt=osasuna",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/granada.png',["alt=granada",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/mallorca.png',["alt=mallorca",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/madrid.png',["alt=madrid",'class'=>'escudo']);?><b/></h2> 
        <h2><b><?=Html::img('@web/images/sociedad.png',["alt=sociedad",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/sevilla.png',["alt=sevilla",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/valladolid.png',["alt=valladolid",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/valencia.png',["alt=valencia",'class'=>'escudo']);?><b/></h2>
        <h2><b><?=Html::img('@web/images/villareal.png',["alt=villareal",'class'=>'escudo']);?><b/></h2> 
    </div>

<div class="container<?= $this->params['fluid'] ? '-fluid' : '' ?> "> 
    <div class="container">
        <div class="text-center">
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-center">
                        <div class="thumbnail">
                            <div class="caption">
                                <p><h2 class='h2header'>ESTADÍSTICAS</h2></p>                           
                                <div class="container-fluid">                                   
                                    <div class="row">
                        
                                        <div class="col-md-4">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h4>Equipos</h4>
                                                    <?= Html::a('Ver', [''], ['class' => 'btn btn-success']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h4>Jugadores</h4>
                                                    <?= Html::a('Ver', [''], ['class' => 'btn btn-success']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="thumbnail thumb">
                                                <div class="caption">
                                                    <h4>Competiciones</h4>
                                                    <?= Html::a('Ver', [''], ['class' => 'btn btn-success']) ?>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
                <hr style="width: 100%; color: black; height: 1px; background-color:black;" />

                <div class="col-md-6">
                    <div class="thumbnail">
                        <h2>CLASIFICACIÓN</h2>
                        <p>de la LIGA</p>
                        <p><?= Html::a('Ver Clasificacion', ['clasificacionLiga/index'], ['class' => 'btn btn-success']) ?></p> 
                    </div>
                </div>
                    
                <div class="col-md-6">
                    <div class="thumbnail">
                        <h2>CALENDARIO</h2>
                        <p>de la LIGA</p>
                        <p><?= Html::a('Ver Partidos', ['partidosLiga/index'], ['class' => 'btn btn-success']) ?></p> 
                    </div>
                </div>
                
        </div>
    </div>
</div>