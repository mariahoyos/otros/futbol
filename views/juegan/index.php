<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Jugadores;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Juegan';
$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->user->isGuest){ ?>
<div class="juegan-index">

    <h1><?= Html::encode($this->title) ?></h1>

<!--    <p>
        <?= Html::a('Create Juegan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_juegan',
            'codigo_partido',
            'codigo_jugador',
            'ha_jugado',
            'lesion',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php } ?>

<?php if (!Yii::$app->user->isGuest){ ?>

<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">

<div class="juegan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Juegan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_juegan',
            
            [
                'attribute'=>'partido',
                'value'=>function($model) {
                return $model->codigoPartido->codigoEquipoCasa->nombre_equipo.' - '.$model->codigoPartido->codigoEquipoFuera->nombre_equipo;}
            ],

            [
                'attribute'=>'jugador',
                'value'=>function($model) {
                return $model->codigoJugador->nombre.' '.$model->codigoJugador->apellidos;}
            ],
            ['label'=>'ha_jugado',
            'format'=>'raw',
            'value' => function($model, $key, $index, $column) { return $model->ha_jugado == 0 ? 'No' : 'Si';}],
            ['label'=>'lesion',
            'format'=>'raw',
            'value' => function($model, $key, $index, $column) { return $model->lesion == 0 ? 'No' : 'Si';}],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
</div>
<?php } ?>