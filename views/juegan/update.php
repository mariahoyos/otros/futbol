<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Juegan */

$this->title = 'Actualizar Juegan: ' . $model->id_juegan;
$this->params['breadcrumbs'][] = ['label' => 'Juegan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_juegan, 'url' => ['view', 'id' => $model->id_juegan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">
<div class="juegan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'momdel_jugadores' => $model_jugadores,
        'model_partidos' => $model_partidos,
    ]) ?>

</div>
</div>