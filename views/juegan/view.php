<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Juegan */

$this->params['breadcrumbs'][] = ['label' => 'Juegan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->title = $model->codigoJugador->nombre.' '.$model->codigoJugador->apellidos.' ha jugado el '.$model->codigoPartido->codigoEquipoCasa->nombre_equipo.' - '.$model->codigoPartido->codigoEquipoFuera->nombre_equipo;
\yii\web\YiiAsset::register($this);

?>
<div class="cabecera">
  <div class="header1"> </div>
  <div class="cuadrado"> </div>
  <div class="triangulo"> </div>
  <div class="header2"> </div>
</div>

<div class="container">
<div class="juegan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id_juegan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_juegan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_juegan',
            'codigo_partido',
            'codigo_jugador',
            'ha_jugado',
            'lesion',
        ],
    ]) ?>

</div>
</div>