<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Partidos;

/* @var $this yii\web\View */
/* @var $model app\models\Juegan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="juegan-form">

    <?php $form = ActiveForm::begin(); ?>

        <?php 
       $listadepartidos= ArrayHelper::map($model_partidos,'codigo_partido',function($model){
         return $model['jornada'].' - '.$model['nombre_equipo_casa'].' - '.$model['nombre_equipo_fuera'];
     });
    ?>
    
    <?= $form->field($model, 'codigo_partido')->dropDownList($listadepartidos)->label('Partidos') ?>

    <?php
    
    $listadejugadores= ArrayHelper::map($model_jugadores, 'codigo_jugador',
            function($model) {
                return $model['nombre'].' '.$model['apellidos'];
            })
    ?>
    
    <?= $form->field($model, 'codigo_jugador')->dropDownList ($listadejugadores)->label('Jugador')?>

    <?= $form->field($model, 'ha_jugado')->textInput() ?>

<!--    $form->field($model, 'lesion')->textInput() -->

<!-- CHECKBOX BUTTON DEFAULT LABEL -->
    <?= $form->field($model, 'lesion')->checkbox(); ?>
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
