<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "juegan".
 *
 * @property int $id_juegan
 * @property int|null $codigo_partido
 * @property int|null $codigo_jugador
 * @property int|null $ha_jugado
 * @property int|null $lesion
 *
 * @property Jugadores $codigoJugador
 * @property Partidos $codigoPartido
 */
class Juegan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juegan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_partido', 'codigo_jugador', 'ha_jugado', 'lesion'], 'integer'],
            [['codigo_jugador', 'codigo_partido'], 'unique', 'targetAttribute' => ['codigo_jugador', 'codigo_partido']],
            [['codigo_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codigo_jugador' => 'codigo_jugador']],
            [['codigo_partido'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::className(), 'targetAttribute' => ['codigo_partido' => 'codigo_partido']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_juegan' => 'Codigo de juegan',
            'codigo_partido' => 'Partido',
            'codigo_jugador' => 'Jugador',
            'ha_jugado' => 'Participacion',
            'lesion' => 'Lesion',
        ];
    }

    /**
     * Gets query for [[CodigoJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugador()
    {
        return $this->hasOne(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador']);
    }

    /**
     * Gets query for [[CodigoPartido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartido()
    {
        return $this->hasOne(Partidos::className(), ['codigo_partido' => 'codigo_partido']);
    }
    
    public function getNombrecompleto(){
        $nombre = $this->codigoJugador->nombre;
        $apellidos = $this->codigoJugador->apellidos;
        return $nombre.$apellidos;
    }
}
