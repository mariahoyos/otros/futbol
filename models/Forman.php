<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "forman".
 *
 * @property int $id_forman
 * @property int|null $codigo_equipo
 * @property int|null $codigo_jugador
 * @property string $fecha_alta
 * @property string|null $fecha_baja
 *
 * @property Equipos $codigoEquipo
 * @property Jugadores $codigoJugador
 */
class Forman extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'forman';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_equipo', 'codigo_jugador'], 'integer'],
            [['fecha_alta', 'fecha_baja'], 'safe'],
            [['codigo_equipo', 'codigo_jugador'], 'unique', 'targetAttribute' => ['codigo_equipo', 'codigo_jugador']],
            [['codigo_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['codigo_equipo' => 'codigo_equipo']],
            [['codigo_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codigo_jugador' => 'codigo_jugador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_forman' => 'Codigo de forman',
            'codigo_equipo' => 'Equipo',
            'codigo_jugador' => 'Jugador',
            'fecha_alta' => 'Fecha de alta',
            'fecha_baja' => 'Fecha de baja',
        ];
    }

    /**
     * Gets query for [[CodigoEquipo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipo()
    {
        return $this->hasOne(Equipos::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[CodigoJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugador()
    {
        return $this->hasOne(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador']);
    }
    
        public function afterFind() {
        parent::afterFind();
        if ($this->fecha_baja != null){ $this->fecha_baja=Yii::$app->formatter->asDate($this->fecha_baja, 'php:d-m-Y');}
        $this->fecha_alta=Yii::$app->formatter->asDate($this->fecha_alta, 'php:d-m-Y');
        
    }
   
    
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          //$this->fecha_publicacion=Yii::$app->formatter->asDate($this->fecha_publicacion, 'php:Y-m-d');
          if ($this->fecha_baja !=null){$this->fecha_baja= \DateTime::createFromFormat("d/m/Y", $this->fecha_baja)->format("Y/m/d");}
          $this->fecha_alta= \DateTime::createFromFormat("d/m/Y", $this->fecha_alta)->format("Y/m/d");
          return true;
    }
}

