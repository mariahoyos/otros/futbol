<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipos".
 *
 * @property int $codigo_equipo
 * @property string $nombre_equipo
 * @property string|null $entrenador
 * @property string|null $estadio
 *
 * @property Forman[] $forman
 * @property Jugadores[] $codigoJugadors
 * @property Partidos[] $partidos
 * @property Partidos[] $partidos0
 */
class Equipos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_equipo'], 'required'],
            [['nombre_equipo', 'estadio'], 'string', 'max' => 35],
            [['entrenador'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_equipo' => 'Codigo del equipo',
            'nombre_equipo' => 'Nombre del equipo',
            'entrenador' => 'Entrenador',
            'estadio' => 'Estadio',
        ];
    }

    /**
     * Gets query for [[Forman]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getForman()
    {
        return $this->hasMany(Forman::className(), ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[CodigoJugadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugadors()
    {
        return $this->hasMany(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador'])->viaTable('forman', ['codigo_equipo' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Partidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos()
    {
        return $this->hasMany(Partidos::className(), ['codigo_equipo_casa' => 'codigo_equipo']);
    }

    /**
     * Gets query for [[Partidos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos0()
    {
        return $this->hasMany(Partidos::className(), ['codigo_equipo_fuera' => 'codigo_equipo']);
    }
}
