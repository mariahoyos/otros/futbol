<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partidos".
 *
 * @property int $codigo_partido
 * @property int|null $codigo_competicion
 * @property string|null $jornada
 * @property string|null $estadio
 * @property string|null $fecha
 * @property string|null $hora
 * @property int|null $codigo_equipo_casa
 * @property int|null $codigo_equipo_fuera
 * @property int|null $resultado_equipo_casa
 * @property int|null $resultado_equipo_fuera
 *
 * @property Goles[] $goles
 * @property Juegan[] $juegan
 * @property Jugadores[] $codigoJugadors
 * @property Competiciones $codigoCompeticion
 * @property Equipos $codigoEquipoCasa
 * @property Equipos $codigoEquipoFuera
 */
class Partidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_competicion', 'codigo_equipo_casa', 'codigo_equipo_fuera', 'resultado_equipo_casa', 'resultado_equipo_fuera'], 'integer'],
            [['fecha', 'hora'], 'safe'],
            [['jornada'], 'string', 'max' => 40],
            [['estadio'], 'string', 'max' => 35],
            [['codigo_competicion'], 'exist', 'skipOnError' => true, 'targetClass' => Competiciones::className(), 'targetAttribute' => ['codigo_competicion' => 'codigo_competicion']],
            [['codigo_equipo_casa'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['codigo_equipo_casa' => 'codigo_equipo']],
            [['codigo_equipo_fuera'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['codigo_equipo_fuera' => 'codigo_equipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_partido' => 'Codigo del partido',
            'codigo_competicion' => 'Competicion',
            'jornada' => 'Jornada',
            'estadio' => 'Estadio',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'codigo_equipo_casa' => 'Equipo de casa',
            'codigo_equipo_fuera' => 'Equipo de fuera',
            'resultado_equipo_casa' => 'Goles Casa',
            'resultado_equipo_fuera' => 'Goles Fuera',
        ];
    }

    /**
     * Gets query for [[Goles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGoles()
    {
        return $this->hasMany(Goles::className(), ['codigo_partido' => 'codigo_partido']);
    }

    /**
     * Gets query for [[Juegan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegan()
    {
        return $this->hasMany(Juegan::className(), ['codigo_partido' => 'codigo_partido']);
    }

    /**
     * Gets query for [[CodigoJugadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugadors()
    {
        return $this->hasMany(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador'])->viaTable('juegan', ['codigo_partido' => 'codigo_partido']);
    }

    /**
     * Gets query for [[CodigoCompeticion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCompeticion()
    {
        return $this->hasOne(Competiciones::className(), ['codigo_competicion' => 'codigo_competicion']);
    }

    /**
     * Gets query for [[CodigoEquipoCasa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipoCasa()
    {
        return $this->hasOne(Equipos::className(), ['codigo_equipo' => 'codigo_equipo_casa']);
    }

    /**
     * Gets query for [[CodigoEquipoFuera]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEquipoFuera()
    {
        return $this->hasOne(Equipos::className(), ['codigo_equipo' => 'codigo_equipo_fuera']);
    }
    
        public function afterFind() {
        parent::afterFind();
        if ($this->fecha != null){ $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d-m-Y');}
        
        
    }
   
    
//    public function beforeSave($insert) {
//          parent::beforeSave($insert);
//          //$this->fecha_publicacion=Yii::$app->formatter->asDate($this->fecha_publicacion, 'php:Y-m-d');
//          if ($this->fecha !=null){$this->fecha= \DateTime::createFromFormat("d/m/Y", $this->fecha)->format("Y/m/d");}
//          return true;
//    }
}
