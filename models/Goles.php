<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "goles".
 *
 * @property int $codigo_gol
 * @property int|null $codigo_jugador_gol
 * @property int|null $codigo_jugador_asistencia
 * @property int|null $codigo_partido
 * @property int|null $minuto
 *
 * @property Jugadores $codigoJugadorAsistencia
 * @property Jugadores $codigoJugadorGol
 * @property Partidos $codigoPartido
 */
class Goles extends \yii\db\ActiveRecord
{
    public $tipo_equipo;
    public $pp;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'codigo_partido', 'minuto'], 'integer'],
            //[['codigo_jugador_asistencia'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codigo_jugador_asistencia' => 'codigo_jugador']],
            //[['codigo_jugador_gol'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['codigo_jugador_gol' => 'codigo_jugador']],
            [['codigo_partido'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::className(), 'targetAttribute' => ['codigo_partido' => 'codigo_partido']],
            ['minuto', 'compare', 'compareValue' => 100, 'operator' => '<', 'type' => 'number','message'=>'El minuto debe ser anterior al minuto 100'],
            ['minuto', 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number','message'=>'El minuto debe ser superior al minuto 0'],
            [['codigo_partido'],'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_gol' => 'Codigo del gol',
            'codigo_jugador_gol' => 'Jugador goleador',
            'codigo_jugador_asistencia' => 'Jugador asistente',
            'codigo_partido' => 'Partido',
            'minuto' => 'Minuto',
        ];
    }

    /**
     * Gets query for [[CodigoJugadorAsistencia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugadorAsistencia()
    {
        return $this->hasOne(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador_asistencia']);
    }

    /**
     * Gets query for [[CodigoJugadorGol]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugadorGol()
    {
        return $this->hasOne(Jugadores::className(), ['codigo_jugador' => 'codigo_jugador_gol']);
    }

    /**
     * Gets query for [[CodigoPartido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartido()
    {
        return $this->hasOne(Partidos::className(), ['codigo_partido' => 'codigo_partido']);
    }

}
