<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "competiciones".
 *
 * @property int $codigo_competicion
 * @property string $nombre
 * @property string|null $ganador
 * @property string|null $tipo
 * @property int|null $num_equipos
 * @property string|null $anio_inicio
 * @property string|null $anio_fin
 *
 * @property Partidos[] $partidos
 */
class Competiciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'competiciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['num_equipos'], 'integer'],
            [['anio_inicio', 'anio_fin'], 'safe'],
            [['nombre', 'ganador', 'tipo'], 'string', 'max' => 35],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_competicion' => 'Codigo de la competicion',
            'nombre' => 'Nombre',
            'ganador' => 'Ganador',
            'tipo' => 'Tipo',
            'num_equipos' => 'Numero de equipos',
            'anio_inicio' => 'Año de inicio',
            'anio_fin' => 'Año de fin',
        ];
    }

    /**
     * Gets query for [[Partidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos()
    {
        return $this->hasMany(Partidos::className(), ['codigo_competicion' => 'codigo_competicion']);
    }
    
    public function afterFind() {
        parent::afterFind();
        if ($this->anio_fin != null){ $this->anio_fin=Yii::$app->formatter->asDate($this->anio_fin, 'php:d-m-Y');}
        $this->anio_inicio=Yii::$app->formatter->asDate($this->anio_inicio, 'php:d-m-Y');
        
    }
   
    
    public function beforeSave($insert) {
          parent::beforeSave($insert);
          //$this->fecha_publicacion=Yii::$app->formatter->asDate($this->fecha_publicacion, 'php:Y-m-d');
          if ($this->anio_fin !=null){$this->anio_fin= \DateTime::createFromFormat("d/m/Y", $this->anio_fin)->format("Y/m/d");}
          $this->anio_inicio= \DateTime::createFromFormat("d/m/Y", $this->anio_inicio)->format("Y/m/d");
          return true;
    }
}
